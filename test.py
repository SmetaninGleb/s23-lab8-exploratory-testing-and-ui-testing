from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome()
driver.implicitly_wait(10) # seconds

driver.get("https://yandex.ru/games/")

# Find and open "Адская Психушка" game

input_elem = driver.find_element(By.CLASS_NAME, 'Textinput-Control')
input_elem.clear()
input_elem.send_keys("Адская Психушка")
input_elem.send_keys(Keys.RETURN)

driver.implicitly_wait(5)

game_elem = driver.find_element(By.CSS_SELECTOR, 'a.play')
game_elem.click()
driver.implicitly_wait(10)
driver.switch_to.window(driver.window_handles[1])
driver.implicitly_wait(10)
assert 'https://yandex.ru/games/app/213215' in driver.current_url

# Find and open "Нубик: Собери Алмазы!" game

driver.switch_to.window(driver.window_handles[0])

input_elem = driver.find_element(By.CLASS_NAME, 'Textinput-Control')
input_elem.clear()
input_elem.send_keys("Нубик: Собери Алмазы!")
input_elem.send_keys(Keys.RETURN)

driver.implicitly_wait(5)

game_elem = driver.find_element(By.CSS_SELECTOR, 'a.play')
game_elem.click()
driver.implicitly_wait(10)
driver.switch_to.window(driver.window_handles[2])
driver.implicitly_wait(10)
assert 'https://yandex.ru/games/app/216384' in driver.current_url

# Checking "Адская Психушка" game age rating

driver.switch_to.window(driver.window_handles[0])

input_elem = driver.find_element(By.CLASS_NAME, 'Textinput-Control')
input_elem.clear()
input_elem.send_keys("Адская Психушка")
input_elem.send_keys(Keys.RETURN)

driver.implicitly_wait(5)

desc_elem = driver.find_element(By.CSS_SELECTOR, 'div.game-card__game-info')
desc_elem.click()
driver.implicitly_wait(5)

age_rating_elem = driver.find_element(By.CSS_SELECTOR, 'div.game-page__age-rating')
assert '18+' in age_rating_elem.text

driver.close()